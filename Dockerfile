FROM openjdk:8-jdk-alpine

COPY target/*.war customerdataapplication.war

ENTRYPOINT ["java","-jar","/customerdataapplication.war"]
